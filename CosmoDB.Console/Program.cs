﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CosmoDB.Console.Model;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;


namespace CosmoDB.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var key = "KL0CPLhH7e8MRRV1DhSkpxrYhIenHtQpuvAG2ev4X3U4m0BMmvSYCdsZufREAPNaLArrbHPFCQhkd1loNUF4OA==";
            var uri = "https://jaguar-cosmos.documents.azure.com:443/";

            /*
                        // Create a new instance of the Cosmos Client
                        using (var client = new CosmosClient("AccountEndpoint=https://jaguar-cosmos.documents.azure.com:443/;AccountKey=KL0CPLhH7e8MRRV1DhSkpxrYhIenHtQpuvAG2ev4X3U4m0BMmvSYCdsZufREAPNaLArrbHPFCQhkd1loNUF4OA==;"))
                        {
                            var DB = client.GetDatabase("ChampionProductCosmoDB");
                            var table = client.GetContainer(DB.Id, "Customer");

                            //loop through all records
                            var sqlString = "Select * from c";
                            var query = new QueryDefinition(sqlString);
                            FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

                            FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                            foreach (var item in queryResult.Resource)
                            {
                                Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
                            }


                            //Get records with 'Smith' as lastname
                            sqlString = "SELECT * FROM c where c.lastName = 'Smith'";
                            query = new QueryDefinition(sqlString);
                            iterator = table.GetItemQueryIterator<Customer>(sqlString);

                            queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                            foreach (var item in queryResult.Resource)
                            {
                                Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
                            }
                        };
            */

            ConnectionPolicy connectionPolicy = new ConnectionPolicy();
            connectionPolicy.ConnectionMode = Microsoft.Azure.Documents.Client.ConnectionMode.Direct;
            connectionPolicy.ConnectionProtocol = Protocol.Tcp;

            // Set the read region selection preference order
            connectionPolicy.PreferredLocations.Add(LocationNames.WestUS);
            connectionPolicy.PreferredLocations.Add(LocationNames.NorthEurope);
            connectionPolicy.PreferredLocations.Add(LocationNames.SoutheastAsia);

            var documentClient = new DocumentClient(new Uri(uri), key, connectionPolicy);
            var Service = new CosmoDB.Console.Service.CustomerService();


            // Add a new customer
            var newCustomer = new Customer()
            {
                customerId = 1
                ,
                lastName = "Nahasapeemapetilon"
                ,
                firstName = "Apu"
                ,
                addresses = new List<Address>()
                {
                    new Address()
                    {
                        addressId = 1
                        ,addressTypeId = 1
                        ,street1 = "Rt 1 Box 252G"
                        ,street2 = null
                        ,city = "Springfield"
                        ,state = "IL"
                        ,zip = "62629"
                    }
                    ,new Address()
                    {
                        addressId = 2
                        ,addressTypeId = 2
                        ,street1 = "3430 Glenmont Ave"
                        ,street2 = "Suite. 3D"
                        ,city = "Springfield"
                        ,state = "IL"
                        ,zip = "62629"
                    }
                }
                ,
                invoices = new List<Invoice>()
                {
                    new Invoice()
                    {
                        invoiceId = 1
                        ,date = DateTime.Parse("2021-05-09")
                        ,amount = decimal.Parse("1337.42")
                        ,paidDate = null
                        ,paidAmount = 0
                    }
                    ,new Invoice()
                    {
                        invoiceId = 2
                        ,date = DateTime.Parse("2021-06-01")
                        ,amount = decimal.Parse("42.13")
                        ,paidDate = DateTime.Parse("2021-06-07")
                        ,paidAmount = decimal.Parse("42.13")
                    }
                }
            };

            var wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer).GetAwaiter().GetResult());


            var newCustomer2 = new Customer()
            {
                customerId = 2
                ,lastName = "Wiggum"
                ,firstName = "Ralph"
                ,addresses = new List<Address>()
                {
                    new Address()
                    {
                        addressId = 3
                        ,addressTypeId = 1
                        ,street1 = "Rt 1 Box 252D"
                        ,street2 = null
                        ,city = "Springfield"
                        ,state = "IL"
                        ,zip = "62629"
                    }
                }
                ,invoices = new List<Invoice>()
                {
                    new Invoice()
                    {
                        invoiceId = 3
                        ,date = DateTime.Parse("2021-01-02")
                        ,amount = decimal.Parse("618.21")
                        ,paidDate = DateTime.Parse("2021-01-07")
                        ,paidAmount = decimal.Parse("100.00")
                    }
                    ,new Invoice()
                    {
                        invoiceId = 4
                        ,date = DateTime.Parse("2021-02-03")
                        ,amount = decimal.Parse("13.24")
                        ,paidDate = null
                        ,paidAmount = 0
                    }
                    ,new Invoice()
                    {
                        invoiceId = 5
                        ,date = DateTime.Parse("2021-03-04")
                        ,amount = decimal.Parse("77.56")
                        ,paidDate = DateTime.Parse("2021-06-07")
                        ,paidAmount = decimal.Parse("77.56")
                    }
                }
            };

            wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer2).GetAwaiter().GetResult());


            var newCustomer3 = new Customer()
            {
                customerId = 3
                ,lastName = "Simpson"
                ,firstName = "Lisa"
                ,addresses = new List<Address>()
                {
                    new Address()
                    {
                        addressId = 3
                        ,addressTypeId = 1
                        ,street1 = "Rt 1 Box 252D"
                        ,street2 = null
                        ,city = "Springfield"
                        ,state = "IL"
                        ,zip = "62629"
                    }
                }
                ,invoices = new List<Invoice>()
                {
                    new Invoice()
                    {
                        invoiceId = 3
                        ,date = DateTime.Parse("2021-03-07")
                        ,amount = decimal.Parse("121.83")
                        ,paidDate = null
                        ,paidAmount = 0
                    }
                }
            };

            wasSuccessfullyAdded = Task.Run(() => Service.CreateCustomerAsync(documentClient, newCustomer3).GetAwaiter().GetResult());


            // Update a customer
            Customer customerToUpdate = documentClient.CreateDocumentQuery<Customer>(UriFactory.CreateDocumentCollectionUri("ChampionProductCosmoDB", "Customer2"))
            .Where(c => c.id == "bdfd87bf-950a-4d84-8c8a-7ac351f0c963")
            .AsEnumerable()
            .FirstOrDefault();

            if (customerToUpdate != null)
            {
                customerToUpdate.firstName = "Homer";
                customerToUpdate.addresses.FirstOrDefault().state = "MO";

                var wasSuccessfullyUpdated = Task.Run(() => Service.UpdateCustomerAsync(documentClient, customerToUpdate)).GetAwaiter().GetResult();
            }


            // Delete a customer
            Customer customerToDelete = documentClient.CreateDocumentQuery<Customer>(UriFactory.CreateDocumentCollectionUri("ChampionProductCosmoDB", "Customer2"))
            .Where(c => c.id == "b137278d-8d36-4325-b3a4-07761959e818")
            .AsEnumerable()
            .LastOrDefault();

            if (customerToDelete != null)
            {
                var wasSuccessfullyDeleted = Task.Run(() => Service.DeleteCustomerAsync(documentClient, customerToDelete)).GetAwaiter().GetResult();
            }
        }
    }
}
